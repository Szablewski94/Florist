package Flowers;

/**
 * Created by RENT on 2017-08-01.
 */
public class Freesia {

    private String kolor;
    private int ilość;

    public Freesia(String kolor, int ilość) {
        this.kolor = kolor;
        this.ilość = ilość;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public int getIlość() {
        return ilość;
    }

    public void setIlość(int ilość) {
        this.ilość = ilość;
    }
}
