package bartekSzablewski;

public class Customer {
    public String name;
    public double cash;


    public Customer(String name, double cash) {
        this.name = name;
        this.cash = cash;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
